The Wall verder afwerken met naar keuze :

- lua/algernon
- PHP

# LAMP (Linux Apache Mysql Php)

Je moet een webserver installeren:

`sudo apt-get install apache2`

Als je naar `localhost` surft, krijg je een default-site van apache te zien,
dewelke op schijf te vinden is in de map `/var/www/html/`.
De configuratie van apache is (zoals van bijna elk standaard Linux-programma)
te vinden in de map `/etc/apache/`.
Om niet altijd als root te werken, kan je de map `/var/www/html` best alle
schrijfbaar maken voor de user waar je normaal mee werkt, b.v. `sudo chown jos:jos /var/www/html`


Je hebt een achterliggende database nodig:

`sudo apt-get install mysql-server`

Tijdens de installatie wordt het root-wachtwoord van de database gevraagd.
Vergeet dit niet! Gebruik b.v. `imma`.


Php:

`sudo apt-get install php5`

Vanaf nu kan je in de HTML-bestanden die door Apache geservet worden, ook
php gebruiken. Sla daarvoor een bestand `test.php` (de extensie is belangrijk!)
op in `/var/www/html` met de inhoud `<?php phpinfo() ?>`.
Als je nu naar `localhost/test.php` surft, krijg je info over php te zien.


PhpMyAdmin (handig om de database te benaderen):

`sudo apt-get install phpmyadmin`

Dit installeert de web-applicatie PhpMyAdmin op je Apache server,
te benaderen via `localhost/phpmyadmin`.
Tijdens de installatie wordt gevraagd om het administratie-passwoord van je database.
Dit heb je eerder ingesteld op `imma`.
Er wordt ook gevraagd om een wachtwoord in te stellen voor de phpmyadmin-applicatie,
kies b.v. `phpmyadmin`. 
Nu zou je op `localhost/phpmyadmin` moeten kunnen aanmelden met user `phpmyadmin` en idem wachtwoord.




