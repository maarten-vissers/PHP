

<!-- als je geen action meegeeft aan een form, wordt standaard de huidige pagina gebruikt. D.i. een POSTBACK. -->

<form method="get">
    <select name="vorm">
        <option value="ovaal">ovale vorm</option>
        <option value="rond">ronde vorm</option>
        <option value="vierkant">vierkante vorm</option>
    </select>
    <select name="kleur">
        <option value="rood">rode kleur</option>
        <option value="blauw">blauw kleur</option>
        <option value="oranje">oranje kleur</option>
    </select>
    <input type="submit">
</form>

<ul>
<?php

// Pas de key v.d. $_GET associatieve array aan zodat de gekozen vorm en kleur getoond worden
// Let ook op de URL in de adresbalk v.d. browser.

echo "<li>" . $_GET['vorm'] . "</li>";
echo "<li>" . $_GET['kleur'] . "</li>";

?>
</ul>
