<?php

// Maak een Array met 3 namen van medeleerlingen.
$array=array("beethoven", "mozart", "vivaldi");

// Itereer er over met een for-loop. Print ze af in een HTML ongenummerde lijst.
foreach($array as $value){
echo "$value <br>";
}

// Itereer er over met een foreach-loop. Print ze af in HTML genummerde lijst.
$a = 0;
foreach($array as $value){
$a++;
echo "$a. $value <br>";
}


$naam = "Willy";

// Itereer met een for-loop over de variable $naam alsof het een array was.
foreach($naam as $value){
echo "$value <br>";
}
?>
